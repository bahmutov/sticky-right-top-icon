/*global module:false*/
module.exports = function (grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    jshint: {
      options: {
        jshintrc: '.jshintrc',
      },
      'default': {
        src: ['*.js']
      }
    },

    // read CSS lint rules
    // https://github.com/stubbornella/csslint/wiki/Rules
    csslint: {
      options: {
        csslintrc: '.csslintrc'
      },
      src: ['*.css']
    },

    concat: {
      css: {
        options: {
          separator: '\n',
          stripBanners: true,
          banner: '/*! <%= pkg.name %> - <%= pkg.version %> */\n\n'
        },
        src: [
          'sticky-right-top-icon.css'
        ],
        dest: 'dist/sticky-right-top-icon.css'
      },
      js: {
        options: {
          separator: ';\n',
          stripBanners: false,
          banner: '/*! <%= pkg.name %> - <%= pkg.version %> ' +
          'built on <%= grunt.template.today("yyyy-mm-dd") %>\n' +
          'author: <%= pkg.author %>, support: @bahmutov */\n\n'
        },
        src: [
          'sticky-right-top-icon.js'
        ],
        dest: 'dist/sticky-right-top-icon.js'
      }
    },

    replace: {
      dist: {
        options: {
          variables: {
            version: '<%= pkg.version %>',
            timestamp: 'timestamp: <%= grunt.template.today() %>'
          },
          prefix: '@@'
        },
        files: {
          'dist/index.html': 'index.html',
          'dist/bower.json': 'bower.json'
        }
      }
    },

    copy: {
      main: {
        files: {
          'dist/README.md': 'README.md',
          'dist/normalize.css': 'bower_components/normalize-css/normalize.css'
        }
      }
    }
  });

  var plugins = require('matchdep').filterDev('grunt-*');
  plugins.forEach(grunt.loadNpmTasks);

  grunt.registerTask('default', ['jshint', 'csslint', 'concat:css', 'concat:js', 'replace', 'copy']);
};